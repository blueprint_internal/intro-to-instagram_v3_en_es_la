function TestRun(target,resources)
{
	this.target = target;
	this.spritesAr = new Array();
	this.resources = resources;
	this.setUpPreloader();
}

TestRun.prototype = {
	
	setUpPreloader: function()
	{
		this.keepAspectRatio = true;
		this.loadedGraphics = 0;
		canvas = document.getElementById("testCanvas");
		stage = new createjs.Stage(canvas);
		stage.enableMouseOver(10);
		this.stage = stage
		borderPadding = 10;

		var barHeight = 20;
		loaderColor = createjs.Graphics.getRGB(61, 92, 155);
		loaderBar = new createjs.Container();

		bar = new createjs.Shape();
		bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();

		imageContainer = new createjs.Container();
		imageContainer.x = 430;
		imageContainer.y = 200;

		loaderWidth = 300;
		stage.addChild(imageContainer);

		var bgBar = new createjs.Shape();
		var padding = 3
		bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);

		loaderBar.x = canvas.width - loaderWidth >> 1;
		loaderBar.y = canvas.height - barHeight >> 1;
		loaderBar.addChild(bar, bgBar);

		stage.addChild(loaderBar);

		manifest = 
		[
			{src: this.resources.background, id: "background"},
			{src: this.resources.hand, id: "hand"},
			{src: this.resources.gridItem1, id: "grid1"},
			{src: this.resources.gridItem2, id: "grid2"},
			{src: this.resources.gridItem3, id: "grid3"},
			{src: this.resources.gridItem4, id: "grid4"},
			{src: this.resources.gridItem5, id: "grid5"},
			{src: this.resources.gridItem6, id: "grid6"},
			{src: this.resources.gridItem7, id: "grid7"},
			{src: this.resources.gridItem8, id: "grid8"},
			{src: this.resources.gridItem9, id: "grid9"}
		];
		this.map = new Array();
		preload = new createjs.LoadQueue(true);
       
		// Use this instead to use tag loading
		//preload = new createjs.PreloadJS(false);

		preload.on("progress", this.handleProgress,this);
		preload.on("complete", this.handleComplete,this);
		preload.on("fileload", this.handleFileLoad,this);
		//"course/en/animations/02_instagram_community/"
		preload.loadManifest(manifest,true);

		createjs.Ticker.setFPS(30);
	},
	
	handleProgress: function(event) {
		bar.scaleX = event.loaded * loaderWidth;
	},

	handleComplete: function(event) {
		var background = this.map[0];
		stage.addChild(background);
		loaderBar.visible = false;
		
	   	this.setupAnimation();
	   	
       	stage.update();
       	var self = this;
       	createjs.Ticker.addEventListener("tick",self.handleTick);
	    
	},

    handleTick: function(event){
    	stage.update();
    },

	handleFileLoad: function(event) {
		this.map.push(new createjs.Bitmap(event.result))
		stage.update();
	},

    setupAnimation: function()
    {
    	this.hand = this.map[1];
    	this.hand.x = 30;
    	this.stage.addChild(this.hand);

    	for(var i=0;i<9;i++)
    	{
    		var temp = this.map[i+2];
    		this.spritesAr.push(temp);
    		stage.addChild(temp);
    	}
    	var gridItemsAr = new Array();


    	
    	this.setSmallGrid();
    	//this.animateToLargeGrid();
    	this.animateHand();
    	stage.update();
    	
    },
    resetAnimation: function()
    {
    	var self = this;
       	createjs.Ticker.addEventListener("tick",self.handleTick);
    	this.setSmallGrid();
    	//this.animateToLargeGrid();
    	this.animateHand();
    	
    },

    setSmallGrid: function()
    {
    	var cnt = 0;
    	var hgt = 126;
    	var columns = 1;
    	for(var i=0;i<9;i++)
    	{
    		cnt+=1
    		var temp = this.spritesAr[i];
	    	temp.scaleX = .4;
	    	temp.scaleY = .4;
	    	temp.x = 29+(65*cnt);
	    	temp.y = hgt+(65*columns);
	    	temp.alpha = 0;
	    	//console.log("smallgrid"+temp)
	    	if(cnt>=3){
	    		cnt=0;
	    		columns+=1;
	    	}
	    	
    	}
    },

    animateHand: function()
    {
    	this.hand.alpha = 0;
    	this.hand.y = 0;
    	this.hand.x = 68;
    	this.hand.alpha = 0;
    	var self = this;
    	createjs.Tween.get(this.hand)
	         .to({alpha:1,x:30},1000,createjs.Ease.backInOut)
	         .call(this.animateToLargeGrid,null,this);

    },

    animateToLargeGrid: function()
    {
    	var cnt = 0;
    	var hgt = 180;
    	var columns = 1;
    	for(var i=0;i<9;i++)
    	{
    		cnt+=1
    		var temp = this.spritesAr[i];
	    	temp.alpha = .8;
	    	var xLoc = 210+(155*cnt);
	    	var yLoc=(155*columns)-145;
	    	createjs.Tween.get(temp)
	         .wait(200*i)
	         .to({alpha:1,x:xLoc,y:yLoc,scaleX:1,scaleY:1}, 400,createjs.Ease.backInOut)
	    	if(cnt>=3){
	    		cnt=0;
	    		columns+=1;
	    	}
	    	
    	}
    },

	playAnimation: function()
	{
		/*
		var self = this;
		createjs.Tween.get(this.logo)
	         .wait(500)
	         .to({alpha:1,x:436,y:260}, 1000,createjs.Ease.elasticOut)
	         .call(function(){self.handleChange()})
	         createjs.Ticker.addEventListener("tick", handleTick);
	         function handleTick(event)
	          {
	          	console.log("tick");
	          	stage.update();

	          }
	    */
	},

	animationComplete: function(event)
	{
		
		//createjs.Ticker.on("tick", myFunction);
		//function myFunction(event) {
		   // event.remove();
		//}
		//debugger
		//createjs.Ticker.removeEventListener("tick", handleTick);
		var self = this;
       	createjs.Ticker.removeEventListener("tick",self.handleTick);
	},

    
	

	resize: function(w,h)
	{
	
	var ow = 871; // your stage width
	var oh = 473; // your stage height

	if (this.keepAspectRatio == true)
	{
	    // keep aspect ratio
	    var scale = Math.min(w / ow, h / oh);
	    stage.scaleX = scale;
	    stage.scaleY = scale;

	   // adjust canvas size
	   stage.canvas.width = ow * scale;
	   stage.canvas.height = oh * scale;
	}
	
	stage.update()
	//this.replayAnimation();
	}

}

